//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#include "Par05DetectorConstruction.hh"
#include <G4Colour.hh>                     // for G4Colour
#include <G4ThreeVector.hh>                // for G4ThreeVector
#include <G4VUserDetectorConstruction.hh>  // for G4VUserDetectorConstruction
#include <G4ios.hh>                        // for G4endl, G4cout
#include <ostream>                         // for operator<<, basic_ostream
#include <string>                          // for allocator, char_traits
#include "G4Box.hh"                        // for G4Box
#include "G4Tubs.hh"                       // for G4Tubs
#include "G4LogicalVolume.hh"              // for G4LogicalVolume
#include "G4Material.hh"                   // for G4Material
#include "G4NistManager.hh"                // for G4NistManager
#include "G4PVPlacement.hh"                // for G4PVPlacement
#include "G4UnitsTable.hh"                 // for operator<<, G4BestUnit
#include "G4VisAttributes.hh"              // for G4VisAttributes
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "Par05FastSimModel.hh"
class G4VPhysicalVolume;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05DetectorConstruction::Par05DetectorConstruction()
  : G4VUserDetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05DetectorConstruction::~Par05DetectorConstruction() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* Par05DetectorConstruction::Construct()
{
  //--------- Material definition ---------
  G4NistManager* nistManager = G4NistManager::Instance();
  G4Material* air            = nistManager->FindOrBuildMaterial("G4_AIR");
  G4Material* pbwo4           = nistManager->FindOrBuildMaterial("G4_PbWO4");

  //--------- Dimensions ---------
  G4double detectorInnerRadius = 80 * cm;
  G4double detectorOuterRadius = detectorInnerRadius + 50 * cm;
  G4double detectorLength = 2 * m;
  G4double worldSizeXY         = detectorOuterRadius * 3.;
  G4double worldSizeZ          = detectorLength * 2;

  //--------- Visualization ---------
  std::array<G4VisAttributes, 2> attribs;
  attribs[0].SetColour(G4Colour(0, 0, 1, 0.1));
  attribs[0].SetForceSolid(true);
  attribs[1].SetColour(G4Colour(1, 0, 0, 0.1));
  attribs[1].SetForceSolid(true);

  //--------- World ---------
  auto solidWorld  = new G4Box("World",                  // name
                                worldSizeXY / 2.,         // half-width in X
                                worldSizeXY / 2.,         // half-width in Y
                                worldSizeZ / 2.);         // half-width in Z
  auto logicWorld  = new G4LogicalVolume(solidWorld,    // solid
                                          air,            // material
                                          "World");       // name
  auto physicWorld = new G4PVPlacement(0,                // no rotation
                                        G4ThreeVector(),  // at (0,0,0)
                                        logicWorld,      // logical volume
                                        "World",          // name
                                        0,                // mother volume
                                        false,            // not used
                                        999,              // copy number
                                        true);            // copy number
  logicWorld->SetVisAttributes(attribs[0]);

  //--------- Detector envelope ---------
  auto solidDetector = new G4Tubs("Detector",               // name
                                   detectorInnerRadius,     // inner radius
                                   detectorOuterRadius,      // outer radius
                                   detectorLength / 2.,     // half-width in Z
                                   0,                        // start angle
				  2 * CLHEP::pi);                 // delta angle
  auto logicDetector = new G4LogicalVolume(solidDetector,  // solid
                                            pbwo4,             // material
                                            "Detector");     // name
  new G4PVPlacement(0,                                       // no rotation
                    G4ThreeVector(0, 0, 0),                  // detector centre at (0,0,0)
                    logicDetector,                          // logical volume
                    "Detector",                              // name
                    logicWorld,                             // mother volume
                    false,                                   // not used
                    99,                                      // copy number
                    true);                                   // check overlaps
  logicDetector->SetVisAttributes(attribs[1]);

  // Region for fast simulation
  auto detectorRegion = new G4Region("DetectorRegion");
  detectorRegion->AddRootLogicalVolume(logicDetector);

  return physicWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void Par05DetectorConstruction::ConstructSDandField()
{
  auto detectorRegion = G4RegionStore::GetInstance()->GetRegion("DetectorRegion");
  // attach fast sim model
  new Par05FastSimModel("model", detectorRegion);
}
