//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#include "Par05FastSimModel.hh"
#include "Par05FastSimModelMessenger.hh"
#include <stddef.h>                      // for size_t
#include <G4FastStep.hh>                 // for G4FastStep
#include <G4FastTrack.hh>                // for G4FastTrack
#include <G4Track.hh>                    // for G4Track
#include <G4VFastSimulationModel.hh>     // for G4VFastSimulationModel
#include "G4Electron.hh"                 // for G4Electron
#include "G4Gamma.hh"                    // for G4Gamma
#include "G4Positron.hh"                 // for G4Positron
#include "G4GDMLParser.hh"
#include "Randomize.hh"
class G4ParticleDefinition;
class G4Region;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05FastSimModel::Par05FastSimModel(G4String aModelName, G4Region* aEnvelope)
  : G4VFastSimulationModel(aModelName, aEnvelope)
  , fMessenger(new Par05FastSimModelMessenger(this))
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05FastSimModel::Par05FastSimModel(G4String aModelName)
  : G4VFastSimulationModel(aModelName)
  , fMessenger(new Par05FastSimModelMessenger(this))
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05FastSimModel::~Par05FastSimModel() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool Par05FastSimModel::IsApplicable(const G4ParticleDefinition& aParticleType)
{
  return &aParticleType == G4Electron::ElectronDefinition() ||
         &aParticleType == G4Positron::PositronDefinition() ||
         &aParticleType == G4Gamma::GammaDefinition();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool Par05FastSimModel::ModelTrigger(const G4FastTrack& aFastTrack)
{
  // no kinematic threshold
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void Par05FastSimModel::DoIt(const G4FastTrack& aFastTrack, G4FastStep& aFastStep)
{
  // Remove particle from further processing by G4
  aFastStep.KillPrimaryTrack();
  aFastStep.SetPrimaryTrackPathLength(0.0);
  G4double energy = aFastTrack.GetPrimaryTrack()->GetKineticEnergy();
  aFastStep.SetTotalEnergyDeposited(energy);

  // Propagate this particle
  // TODO
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void Par05FastSimModel::ReadFile() {
  G4cout << " Reading simplified geometry from " << fGdmlName << G4endl;
  G4GDMLParser parser;
  parser.Read( fGdmlName );
  // Get the top volume
  G4VPhysicalVolume * topVolume = parser.GetWorldVolume();
  // Probably we need to create a navigator (or attach the top volume from above)
  // TODO ...
}
