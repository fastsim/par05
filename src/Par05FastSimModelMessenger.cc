//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#include "Par05FastSimModelMessenger.hh"
#include <CLHEP/Units/SystemOfUnits.h>   // for pi
#include <G4ApplicationState.hh>         // for G4State_Idle
#include <G4UImessenger.hh>              // for G4UImessenger
#include <string>                        // for stoi
#include "G4UIcmdWithAnInteger.hh"       // for G4UIcmdWithAnInteger
#include "G4UIcmdWithAString.hh"       // for G4UIcmdWithAString
#include "G4UIdirectory.hh"              // for G4UIdirectory
#include "Par05FastSimModel.hh"     // for Par05FastSimModel
class G4UIcommand;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05FastSimModelMessenger::Par05FastSimModelMessenger(Par05FastSimModel* aModel)
  : G4UImessenger()
  , fModel(aModel)
{
  fExampleDir = new G4UIdirectory("/Par05/");
  fExampleDir->SetGuidance("UI commands specific to this example");

  fSimpleGeoDir = new G4UIdirectory("/Par05/simple_geo/");
  fSimpleGeoDir->SetGuidance("Simplified geometry UI commands");

  fGdmlNameCmd = new G4UIcmdWithAString("/Par05/simple_geo/filename", this);
  fGdmlNameCmd->SetGuidance("Set the name of the GDML file");
  fGdmlNameCmd->SetParameterName("Name", false);
  fGdmlNameCmd->AvailableForStates(G4State_Idle);
  fGdmlNameCmd->SetToBeBroadcasted(true);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Par05FastSimModelMessenger::~Par05FastSimModelMessenger()
{
  delete fGdmlNameCmd;
  delete fSimpleGeoDir;
  delete fExampleDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void Par05FastSimModelMessenger::SetNewValue(G4UIcommand* aCommand, G4String aNewValue)
{
  if(aCommand == fGdmlNameCmd)
  {
    fModel->SetGdmlName(aNewValue);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4String Par05FastSimModelMessenger::GetCurrentValue(G4UIcommand* aCommand)
{
  G4String cv;
  if(aCommand == fGdmlNameCmd)
  {
    cv = fModel->GetGdmlName();
  }
  return cv;
}

