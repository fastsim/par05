-------------------------------------------------------------------

     =========================================================
     Geant4 - an Object-Oriented Toolkit for Simulation in HEP
     =========================================================

                            Example Par05
                            -------------

 This example demonstrates how to use simplified geometry for quick propagation of particles.

 1. Detector description
 -----------------------

 The world volume is composed of a single cylinder with the envelope of the detector.
 In reality this geometry will be a complex one, and its simplified picture will be
 firstly read in from GDML, and then used (via un-registered parallel world) in the
 propagation of particles.

 2. Primary generation
 ---------------------

 Particle gun is used as a primary generator. 10 GeV electron is used by default.
 By default particles are generated along y axis. Those values
 can be changed using /gun/ UI commands.

 3. Physics List
 ---------------

 FTFP_BERT modular physics list is used.  On top of it, fast simulation physics
 is registered for selected particles (electrons, positrons).

 4. User actions
 ----------------------------------------------------------

 - Par04RunAction : run action used for initialization and termination
                    of the run. Histograms for analysis of shower development
                    in the detector are created.

 - Par04EventAction : event action used for initialization and termination
                      of the event. Analysis of shower development is performed
                      on event-by-event basis.

 5. Fast simulation
 ---------------

 Fast simulation model is used to take over simulation from Geant4. In this example
 such particles are propagated within Geant4 using a simplified geometry.
 Simplified geometry is loaded from GDML, and its top volume is used by a custom navigator.
 Only several points on the particle trajectory are stored as output (TODO).

 6. How to build and run the example
 -----------------------------------

- Compile and link to generate the executable (in your CMake build directory):
  % cmake <Par05_SOURCE>
  % make

- Execute the application (in batch mode):
  % ./examplePar05 -m examplePar05.mac
  which produces two root files for full simulation.

- Execute the application (in interactive mode):
  % ./examplePar05 -i -m vis.mac
  which allows to visualize the detector and particles

 7. Macros
 ---------

 vis.mac - Allows to run visualization. Pass it to the example in interactive mode ("-i" passed to the executable).
           It can be used to visualize the simulation.

 examplePar05.mac - Runs full simulation. It will run 100 events with single electrons, 10 GeV and
                   along y axis.


 8. UI commands
 --------------

 UI commands useful in this example:

- particle gun commands
   /gun/particle e-
   /gun/energy 10 GeV
   /gun/direction 0 1 0
   /gun/position 0 0 0
