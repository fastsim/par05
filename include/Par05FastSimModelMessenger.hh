//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#ifndef PAR05FASTSIMMODELMESSENGER_H
#define PAR05FASTSIMMODELMESSENGER_H

#include <G4String.hh>           // for G4String
#include "G4UImessenger.hh"      // for G4UImessenger
class G4UIcmdWithAString;
class G4UIcommand;
class G4UIdirectory;
class Par05FastSimModel;

/**
 * @brief Dummy fast sim model messenger.
 *
 * Provides UI commands to setup the dummy model: number of cells to be created.
 *
 */

class Par05FastSimModelMessenger : public G4UImessenger
{
 public:
  Par05FastSimModelMessenger(Par05FastSimModel*);
  ~Par05FastSimModelMessenger();
  /// Invokes appropriate methods based on the typed command
  virtual void SetNewValue(G4UIcommand*, G4String) final;
  /// Retrieves the current settings
  virtual G4String GetCurrentValue(G4UIcommand*) final;

 private:
  /// Inference to setup
  Par05FastSimModel* fModel = nullptr;
  /// Command to set the directory common to all inference messengers in this example
  /// /Par04
  G4UIdirectory* fExampleDir = nullptr;
  /// Command to set the directory for fast sim model settings /Par05/simple_geo
  G4UIdirectory* fSimpleGeoDir = nullptr;
  /// Command to set the filename
  G4UIcmdWithAString* fGdmlNameCmd = nullptr;
};

#endif
