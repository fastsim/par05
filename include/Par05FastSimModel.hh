//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#ifndef PAR05FASTSIMMODEL_HH
#define PAR05FASTSIMMODEL_HH

#include <G4String.hh>                // for G4String
#include <G4ThreeVector.hh>           // for G4ThreeVector
#include <G4Types.hh>                 // for G4bool, G4double
#include <memory>                     // for unique_ptr
#include <vector>                     // for vector
#include "G4VFastSimulationModel.hh"  // for G4VFastSimulationModel
class G4FastTrack;
class G4ParticleDefinition;
class G4Region;
class Par05FastSimModelMessenger;

/**
 * @brief Fast simulation model for quick particle propagation
 *
 **/

class Par05FastSimModel : public G4VFastSimulationModel
{
 public:
  Par05FastSimModel(G4String, G4Region*);
  Par05FastSimModel(G4String);
  ~Par05FastSimModel();
  /// There are no kinematics constraints. True is returned.
  virtual G4bool ModelTrigger(const G4FastTrack&) final;
  /// Model is applicable to electrons, positrons, and photons.
  virtual G4bool IsApplicable(const G4ParticleDefinition&) final;
  /// Take particle out of the full simulation and propagate it
  /// through simplified geometry
  virtual void DoIt(const G4FastTrack&, G4FastStep&) final;
  /// Setter fGdmlName
  inline void SetGdmlName(G4String aName) {
    fGdmlName = aName;
    ReadFile();
  };
  /// Getter fGdmlName
  inline const G4String GetGdmlName() {return fGdmlName;};
  /// Read GDML
  void ReadFile();

 private:
  // Messenger
  Par05FastSimModelMessenger* fMessenger;
  // Name of the input GDML with simplified geometry
  G4String fGdmlName;
};
#endif /* PAR05FASTSIMMODEL_HH */
