//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#ifndef PAR05DETECTORCONSTRUCTION_H
#define PAR05DETECTORCONSTRUCTION_H

#include <CLHEP/Units/SystemOfUnits.h>     // for cm, mm, pi, rad
#include <G4String.hh>                     // for G4String
#include <G4Types.hh>                      // for G4double, G4bool, G4int
#include <array>                           // for array
#include <cstddef>                         // for size_t
#include <vector>                          // for vector
#include "G4Material.hh"                   // for G4Material
#include "G4SystemOfUnits.hh"              // for cm, mm, rad
#include "G4ThreeVector.hh"                // for G4ThreeVector
#include "G4VUserDetectorConstruction.hh"  // for G4VUserDetectorConstruction
class G4LogicalVolume;
class G4VPhysicalVolume;
class Par05DetectorMessenger;

/**
 * @brief Detector construction.
 *
 * Creates a world volume. There is no detailed geometry which normally exists in experiments.
 *
 */

class Par05DetectorConstruction : public G4VUserDetectorConstruction
{
 public:
  Par05DetectorConstruction();
  virtual ~Par05DetectorConstruction();

  virtual G4VPhysicalVolume* Construct() final;
  virtual void ConstructSDandField() final;

};

#endif /* PAR05DETECTORCONSTRUCTION_H */
